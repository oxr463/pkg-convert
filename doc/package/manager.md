# Package Managers

| Package Manager    | Total Packages | Initial Release | Operating System             |
| -------------------| --------------:| ---------------:| ----------------------------:|
| [apk][apk]         |           4529 |            2008 | [Alpine Linux][alpine]       |
| [apt][apt]         |          51000 |            1998 | [Debian Linux][debian]       |
| [opkg][opkg]       |              ? |            2009 | [OpenMoko Linux][openmoko]   |
| [pacman][pacman]   |          87823 |            2002 | [Arch Linux][arch]           |
| [pkgng][pkgng]     |          29758 |            2011 | [FreeBSD][freebsd]           |
| [portage][portage] |          19000 |            1999 | [Gentoo Linux][gentoo]       |
| [rpm][rpm]         |              ? |            1997 | [Red Hat Linux][redhat]      |
| [termux][termux]   |           1035 |            2015 | [Android][android]           |

**Note: [rpm][rpm] is technically the standard format for packages according to the [Linux Standard Base (LSB)][lsb].**

[alpine]: https://alpinelinux.org
[android]: https://android.com
[apk]: https://wiki.alpinelinux.org/wiki/Alpine_Linux_package_management
[apt]: https://wiki.debian.org/Apt
[arch]: https://www.archlinux.org
[debian]: https://www.debian.org
[freebsd]: https://www.freebsd.org
[gentoo]: https://www.gentoo.org
[lsb]: https://en.wikipedia.org/wiki/Linux_Standard_Base
[openmoko]: https://en.wikipedia.org/wiki/Openmoko_Linux
[opkg]: http://git.yoctoproject.org/cgit/cgit.cgi/opkg
[package-managers]: https://en.wikipedia.org/wiki/Package_manager
[pacman]: https://www.archlinux.org/pacman
[pkgng]: https://wiki.freebsd.org/pkgng
[portage]: https://wiki.gentoo.org/wiki/Project:Portage
[redhat]: https://www.redhat.com
[rpm]: http://rpm.org
[termux]: https://termux.com
