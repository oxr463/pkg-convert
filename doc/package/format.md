# Package Format

## Stages

Each template has been divided into the following stages:

| Name            | Description                                     |
| --------------- | ----------------------------------------------- |
| `definitions`   | Environment variables, Makefile variables, etc. |
| `configuration` | `./bootstrap.sh`, `configure`, etc.             |
| `compilation`   | `make`                                          |
| `verification`  | `make check`                                    |
| `installation`  | `make install`                                  |

## Schema

| Property        | Required |
| --------------- | --------:|
| `architecture`  | false    |
| `category`      | false    |
| `checksum`      | true     |
| `dependencies`  | false    |
| `description`   | false    |
| `license`       | true     |
| `maintainer`    | false    |
| `name`          | true     |
| `release`       | true     |
| `source`        | true     |
| `url`           | true     |
| `version`       | true     |

Note: see [`schema.yml`](../../template/schema.yml) for more information.

## Reference

- [Package Format](https://en.wikipedia.org/wiki/Package_format)

