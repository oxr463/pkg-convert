#!/bin/sh
# Convert between various package formats
# SPDX-License-Identifier: GPL-3.0-or-later
set -eu

check_deps() {
  deps="mustach"

  for dep in $deps; do
    if [ -z "$(command -v "${dep}")" ]; then
      exit 125
    fi
  done
}

main() {
  check_deps
}

if [ "$(basename "$0")" = "pkg-convert" ]; then
  main "$@"
fi
